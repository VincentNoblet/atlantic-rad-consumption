 ATLANTIC-RAD-CONSUMPTION

atlantic-rad-consumption is a an internal data mining project developed in [Python3] by the Atlantic group. From now on, atlantic-rad-consumption will be called atlantic for convenience. Built as a CRIPS-DM, it aims to extract knowledge from electrical heating consumption of housing equipped with connected devices. atlantic contains 3 main functionnalities:

  - Process datasets from devices' data
  - Build a matrix of features describing a house and winter phenomenas identified by metier knowledges
  - Estimate electrical heating consumption 
 

# Services

  - preprocessing
  - analyze
  - dash_report
  - powerbi_simulator


### Tech

atlantic uses a number of open source projects to work properly:

* [Luigi] - Automating workflows using the luigi batch workflow system
* [d6tflow] - Software layer onto luigi to automate data workflows
* [Dash] - Framework for building analytical web application


### Installation

atlantic requires Python3.7 to run. It can be run either on Windows (on which it has been implemented) or a UNIX-based OS. The following procedures will be described for Ubuntu. You need root access for few install prerequisites.
We highly recommand to use python 3.7 in order to replicate the environnment as it has been built. Otherwise, you may end up to deal with package versioning to fix.
We recommend to create a virtual machine with Ubuntu 18.04 LTS, for it comes with python3.7.

#####  Create Virtual Machine (optional)
Got to https://console.cloud.google.com/compute/instances?project=atlantic-rad
Go to Create an instance
Choose a name, a region, the image of the OS distribution (Ubuntu 18.04 LTS)
Choose a service account that have sufficient right for the atlantic-rad project (for BigQuery purposes)
Enable HTTP AND HTTPS traffic

Once the VM start, you can ssh it via any computer by adding your public ssh key to https://console.cloud.google.com/compute/metadata/sshKeys?project=atlantic-rad or you can access it directly in the browser (see SSH button at https://console.cloud.google.com/compute/instances?project=atlantic-rad)

####  Install python, virtualenv on root


```sh
$ sudo apt-get install python3.7
$ sudo apt-get install python3.7-dev libpq-dev build-essential
$ sudo apt-get update
$ sudo apt-get install virtualenv
```

Clone and cd the repository
```sh
$ sudo apt-get install git
$ git clone https://gitlabce.groupe-atlantic.com/data/atlantic_rad_consumption
$ cd atlantic_rad_consumption
```
Set up virtual environment and python packages
```sh
$ virtualenv venv -p /usr/bin/python3.7
$ source venv/bin/activate
$ python3.7 -m pip install -r requirements.txt 
$ deactivate
```

### CONFIGURATION

The config.py file takes the config.yml to setup project configurations.
See comments for more informations. Please make sure luigi daemon configurations are right before launching the preprocessing service.
The configuration of the GCP datasets name - like project name, table name - can be found hard-coded in the manager.bigquery_structure.py file. If dataset names change, modify the attributes of the EasyTableName class. All table references points on these attributes.

### AUTHENTICATION
If you use a Google VM, you may always choose a service account to authenticate (the VM must be down).
In all cases, you can authenticate thanks to a JSON authentication file and set its path as an environnement variable (see https://cloud.google.com/docs/authentication/getting-started?hl=fr)
You will know if you authenticate with a suitable account when launching services.

### Preprocessing
The preprocess is a succession of operations which has BigQuery tables as its input of datas and a local parquet dataframe as its outptut. The output dataframe summarizes winter historical features and structural static features each year for each house that produces consumption-related datas
The output is the output of the last d6tflow task: TaskMergeFeatureAll.
You can check for its local parquet file in the folder or you can find it the atlantic_rad_consumption bucket at model_utils/TaskMergeFeatureAll.pq

Please, make sure configurations are ok when running a service.
If you are not running on a UNIX-based OS, please set luigi.daemon.activate = False in the config.yml file.
Launch the preprocess workflow:

```sh
$ source venv/bin/activate
$ python3.7 -m preprocessing
$ deactivate
```

The historical features are treated in a pipeline of luigi tasks. It means that they have independent successions of tasks. These tasks are bigquery tasks. You can find the list of pipeline in the preprocessing.local_task_historical.py file. The typical chain of task is: get the data from rawdevicestate or openwheathermap, lead over time, filter aberrant duration time, filter aberrant value, group by winter partition, aggregate, ratio with winter duration, group by gateway, aggregate.

The four table of static features are also preprocess independentely (device, heater, room, setup). First a bigquery preprocess and then a local one. The static tables are merged and new features are computed from these tables.

Some other sources of datas like isolation, model features, etc... are just loaded by different tasks independentely.

Once this 3 source types of datas are terminated. They are merged (full outer join) thanks to the TaskMergeFeatureAll task.

##### Preprocessing configuration and options
You can follow the execution process at the the external ip address of the VM at a specific port via the http protocol (see the configuration section)
If you are running locally on your machine, the ip address should be localhost (127.0.0.1)
Please verify in the config.yml file that the internal IP is set on the internal IP address of the Google VM if running one or to localhost if running locally. (luigi.daemon.scheduler_host_ip) We advise to set the port at 8082 (luigi.daemon.scheduler_host_port)
Set the number of thread to parallelize variable processes (luigi.workers). It is advised to set lui.workers=number of historical variables to fully enjoy the pipeline tasks' parallelization.

If you wish to invalidate all d6tflow's tasks and launch preprocessing from the beginning:

```sh
$ source venv/bin/activate
$ python3.7 -m preprocess invalidate_upstream
$ deactivate
```

If you wish to add datas in an other winter, go in the config.yml file and add an element in the variable.partition_winter list and the variable.partition_year list:

    partition_winter: 
        2020:
            begin_date: 2019-10-01
            end_date: 2020-03-31
        (...)
            
     partition_year: 
        2020:
            begin_date: 2019-8-20
            end_date: 2020-8-19
        (...)
### Analyze
Analyze trains a model on a part of the TaskMergeFeatureAll dataset. It calls the task TaskMergeFeature  which outputs is loaded remotely in the atlantic_rad_consumption bucket (model_utils/TaskMergeFeature.pq).
The clean version of the TaskMergeFeature output is called TaskMergeFeatureClean (model_utils/TaskMergeFeatureClean.pq). It contains no NA value, it is only numerical value, so it is ready to be the input of a model. Though, this matrix is not normalized.
The model is also sent to GCS in the atlantic-rad-consumption bucket (model_utils/model.pkl).

Launch analyze
```shPreprocessing
$ source venv/bin/activate
$ python3.7 -m analyze
$ deactivate
```

##### Analyze configuration and options


```sh
$ source venv/bin/activate
#You can choose a model (easy to modify: see analyze.__main__.py)
$ python3.7 -m analyze mlp
$ python3.7 -m analyze forest
#You can update feature if something changed (launch TaskMergeFeature and rerun the d6tflow graph only on selected features)
#If running for the first time, update_feature is mandatory
$ python3.7 -m analyze forest update_feature 
$ deactivate
```
Please feel free to visite the analyze.__main__.py file to modify features, see:

    feature_dict_filter = {
        'consumption': {'min': 0, 'max': 10000000},
        'setup_Superficie': {'min': 20, 'max': 350},
        'comfort_temperature': {'min': -np.inf, 'max': np.inf},
        'comfort_range_time': {'min': 0, 'max': 1},
        'isolation': {'min': 1000, 'max': 100000},
        'main_temp': None
    }
Feature must be one of the TaskMergeFeatureAll dataset. If you remove or add one and launch analyze, the powerbi_simulator script will not be valid. You only need to change the powerbi_simulator section in the config.yml file and modify the powerbi_simulator script so the new features can match.
update_feature option calls the method update_feature of the class FeatureFactory of the analyze.analyze_plot.py file. Some filters are hard coded independently of the feature chosen if you wish to have a look like the consumption level or the basic rate.

    def update_feature(feature_dict_filter, divide_superficie=False, normalize=True):
        task_merge = TaskMergeFeature(feature_list=feature_list + ['dataflow', "basic"])
        d6tflow.preview(task_merge)
        d6tflow.run(task_merge)
        data = task_merge.output().load()
     
        (...)
        filter_list = []
        filter_list = self.add_evaluate_filter(data, filter_list, self.custom_filter)
        filter_list = self.add_evaluate_filter(data, filter_list, self.consumption_filter, **dict(min_wh_m2=7000 * 20 ** (1 - divide_superficie), max_wh_m2=350000 * 350 ** (1 - divide_superficie)))
        filter_list = self.add_evaluate_filter(data, filter_list, self.dataflow_filter, **dict(dataflow_rate=0.8))
        filter_list = self.add_evaluate_filter(data, filter_list, self.basic_filter, **dict(min_rate=0, max_rate=0.4))
        filter_list = self.add_evaluate_filter(data, filter_list, self.superficie_filter, **dict(min_m2=20, max_m2=350))
        filter_list = self.add_evaluate_filter(data, filter_list, self.empty_setup_filter)
        #filter_list = self.add_evaluate_filter(data, filter_list, self.maj_model_filter)


### Dash_report

Runs locally the dash_report to see unidimensional variation of the model located in the atlantic_rad_consumption bucket.
You are more likely to run it on a Windows machine, so next command is a batch script.

```bat
$ venv\Scripts\activate
$ py -m dash_report
$ deactivate
```

##### Dash_report configuration and options
Again if you modify features when launching analyze, visit the dash_report.components.scenario.py and modify the dict space_range of theh PredictionGraph class so it matches current feature. This dict permits to limit the visualisation of the prediction curve.

    class PredictionGraph:
        def __init__(self):
            self.space_range = {
                        "comfort_temperature": (17, 22),
                        "isolation": (8000, 52000),
                        "comfort_time_temperature": (0.15, 0.65),
                        "setup_Superficie": (40, 150),
                        "main_temp": (5, 10.5)
                    }

### Powerbi_simulator

Downloads model utilities from the atlantic_rad_consumption bucket so it can be loaded by the local script of the powerbi_simulator/Predict_conso.pbix report. Make sure the python script takes the virtualenv environment (requirements.txt) as its environment.
You are more likely to run it on a Windows machine, so next command is a batch script.

```bat
$ venv\Scripts\activate
$ py -m powerby_simulator
$ deactivate
```

##### Powerbi_simulator configuration and options

The Powerbi_simulator points on legacy model dependencies. See model_utils_legacy/ in the atlantic_rad_consumption bucket


   [Luigi]: <https://github.com/spotify/luigi>
   [d6tflow]: <https://github.com/d6t/d6tflow>
   [Dash]: <https://github.com/plotly/dash>
   [Python3]: <https://www.python.org/>
 